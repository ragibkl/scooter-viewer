# Part 3: Cover Letter (Optional)

## Question

Let us know in a cover letter what excites you right now about engineering or working on the Beam engineering team.

## Answer

Working with code and software is fun. Ever since I took up my first programming job, I have never had to work a single day. Sometimes, I can get lost debugging a specific issue, or optimizing a specific computation path. Hours would pass before I could realize it. I could also spend hours in front of my computer screen tinkering with a different server setup or hosting solutions.

People in software industry generally have good culture. Although my education background is not in computer science, I find myself welcomed by my peers. I also find that software engineers are generally very friendly and approacable. Some engineers I've met are very talented, yet more humble. Regardless of their skill levels, most engineers I've met are always open and eager to learn more and pick up new skills. I also find that talented engineers are more than happy to share their knowledge, either by personal teaching or giving talks. When solving problems, software engineers generally want to find the best, most correct, most feasable solution.

Knowledge in software is quite accessible. Perhaps a few decades ago, one would have to take formal classes to learn specific skills such as in programming or system management. Now, most learning materials are available online and most of the time for free. If I wanted to learn something, I just type a few keywords on Google or Youtube and start learning away. Paid premium courses are also available at reasonable prices.

Technology in software engineering is always changing and growing rapidly. Some people find that to be tiring, but I find that to be exciting. It is always fun to find new ways to do things better. I started doing basic web hosting on plain Linux. I've from that, into Docker, Docker Compose, into a Docker Swarm Cluster, and now on Kubernetes. Since I last worked with React seriously, they added a couple more features such as Context and useState. RustLang is at full release, and is getting wide attention from the community. It is good to always new ways of working and solving problems better.

Working in software allows me to work across multiple domain expertise and disciplines. Sofware runs in marketing, media, job portals, commerce, finance, and many others. It opens up a lot of opportunity to explore various fields.

Working in software is very rewarding and satisfying. Every hour of our working time is spent on putting useful software in the hands of users. I like to think that every piece of code that I write, is helping someone. Most often than not, our work adds value to the world.
